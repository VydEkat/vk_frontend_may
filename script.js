const floorSelect = document.getElementById("floor");
const roomSelect = document.getElementById("room");

for (let i = 3; i <= 27; i++) {
    const option = document.createElement("option");
    option.value = i;
    option.textContent = i;
    floorSelect.appendChild(option);
}

for (let i = 1; i <= 10; i++) {
    const option = document.createElement("option");
    option.value = i;
    option.textContent = i;
    roomSelect.appendChild(option);
}

document.getElementById("submit").addEventListener("click", () => {
    const formData = {
        tower: document.getElementById("tower").value,
        floor: document.getElementById("floor").value,
        room: document.getElementById("room").value,
        date: document.getElementById("date").value,
        time: document.getElementById("time").value,
        comment: document.getElementById("comment").value
    };
    console.log(JSON.stringify(formData));
});

document.getElementById("reset").addEventListener("click", () => {
    document.getElementById("bookingForm").reset();
});